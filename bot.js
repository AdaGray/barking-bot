// BarkingBot by BarkingDog#4975; using discord.js 11.3.2; git: BarkingDogMc/barking-bot; hosted on heroku.
const Discord = require("discord.js");
const bot = new Discord.Client();
const fs = require("fs");
const prefix = (process.env.BOT_PREFIX) // Defining a bunch of stuff ^

// On ready
bot.on("ready", () => {
  console.log("BarkingBot was started!"); // Sends a boot message to console
  bot.user.setActivity('Prefix: ' + prefix + ' | ' + prefix + 'help', {type: "PLAYING"}); // Sets playing
  bot.user.setStatus('online') // Sets status
});

// Command loader
bot.commands = new Discord.Collection();

fs.readdir('./commands/', (err, files) => {  // Reads files in /commands/
  if(err) console.error(err); // Sends errors to console

  let cmds = files.filter(f => f.split('.').pop() === 'js'); // Contains .js files

  if(cmds.lenght <= 0) { // Checks for available commands
    return console.log('No commands to load!'); // If no commands, sends error
  }

  console.log(`Loading ${files.lenght} commands...`); // Logs how many commands are loaded

  cmds.forEach((f, i) => { // Executes for every file, f=files,i=number
    const command = require(`./commands/${f}`); // Require file with command
    console.log(`${i + 1}: ${f} loaded!`); // Logs loaded commands
    bot.commands.set(command.help.name, command); // Pushes files to collection
  });
});

// Reacts to commands
bot.on("message", message => {
const messageArray = message.content.split(/\s+/g); // Splits messages
const command = messageArray[0]; // Command with prefix
const args = messageArray.slice(1); // Arguments given to command
let cmd = bot.commands.get(command.slice(prefix.length)); // Gets command from our collection

if(command.startsWith(prefix)){ // If command object starts with prefix
  if(cmd) cmd.run(bot, message, args); // If cmd object isn't null, execute command code
}
});

// Basic test command
bot.on("message",msg => {
    if (msg.content.startsWith(prefix + "test")) {
        msg.channel.send("The bot is up and is able to respond to commands.");
    }
});

// Unflip
bot.on('message', message => {
  if (message.content.includes('(╯°□°）╯︵ ┻━┻')) {
    message.channel.send('┬─┬﻿ ノ( ゜-゜ノ)')
  }
})

// Login
bot.login(process.env.BOT_TOKEN)
