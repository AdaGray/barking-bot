# TO-DO
### To do list for BarkingBot

###### Basic:
- [x] Add eval `eval code`
- [x] Add unflip
- [x] Add say `say text`
- [x] Add help `help`
- [x] Mass-delete `clear #`
- [x] Add ping `ping`
- [x] Add meme `meme`
- [ ] Greeting messages
- [x] Basic moderation `ban @user` `kick` `softban` `mute`
- [x] Yes/No Polls `poll q`

###### Normal:
- [ ] Advanced moderation `warn @user`
- [ ] UD integration `ud life`
- [ ] Advanced polls `poll q|c1|c2`
- [x] Random anime gif `slap @user` `hug` etc.

###### Advanced (impossible):
- [ ] Command aliases `ban` => `b`
- [ ] Image search `randomdog`
- [ ] Economy system
- [ ] Selfroles `rolelist` `giverole blue`
- [ ] Leveling system
- [ ] Moderation log in a channel
- [ ] Data storage (for warns, economy, etc.)