# Contributing

### How to contribute
If you want to contribute to this bot, simply make a merge request.

### Rules for contributing
The changes you make to the bot should not be malicious, should be SFW and should not give you any more access to the bot.
If you want to update the gifs/memes, look at the images.json file.