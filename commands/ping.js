const Discord = module.require('discord.js')

module.exports.run = async (bot, message, args) => {
  let embed = new Discord.RichEmbed()
  .setColor(0x005eff)
  .setDescription(`Pong! :ping_pong: ${Math.round(bot.ping)}ms`);
  return message.channel.send(embed);
};

exports.help = {name: 'ping'}