const Discord = module.require('discord.js');
const OwnerID = (process.env.BOT_OWNER);
const prefix = (process.env.BOT_PREFIX)

module.exports.run = async (bot, message, args) => {
  if(!message.member.hasPermission("MANAGE_MESSAGES")) return message.reply("You are not a moderator");
  if(!args[0]) return message.reply("Usage: _<" + prefix + "clear #>_");
  message.channel.bulkDelete(args[0]).then(() => {
    message.channel.send('Messages cleared.').then(msg => msg.delete(250))
  })
}

exports.help = {name: "clear"}