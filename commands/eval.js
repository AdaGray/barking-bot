const Discord = module.require('discord.js');
const OwnerID = (process.env.BOT_OWNER);
const BotToken = (process.env.BOT_TOKEN);

module.exports.run = async (bot, message, args) => {
  if(message.author.id !== OwnerID) return;
  try {
    const code = args.join(" ");
    let evaled = eval(code);

    if (typeof evaled !== "string")
      evaled = require("util").inspect(evaled);

    message.channel.send(clean(evaled));
  }
  catch (err) {
    console.log('Error while executing eval.')
  }
};

// Prevents mentions
function clean(text) {
  if (typeof(text) === "string")
    return text.replace(/`/g, "`" + String.fromCharCode(8203)).replace(/@/g, "@" + String.fromCharCode(8203));
  else
      return text;
}

exports.help = {name: "eval"}
