const Discord = module.require('discord.js');
const OwnerID = (process.env.BOT_OWNER);
const prefix = (process.env.BOT_PREFIX)

module.exports.run = async (bot, message, args) => {
    const embed = new Discord.RichEmbed()
    .setColor(0x005eff)
    .setTitle('Help')
    .addField(prefix + 'help', 'Sends this message: _<' + prefix + 'help>_')
    .addField(prefix + 'clear', 'Bulk deletes messages: _<' + prefix + 'clear #>_')
    .addField(prefix + 'test', 'Sends a basic test message: _<' + prefix + 'test>_')
    .addField(prefix + 'hug', 'Sends a random anime hug image: _<' + prefix + 'hug @user>_')
    .addField(prefix + 'kiss', 'Sends a random anime kiss image: _<' + prefix + 'kiss @user>_')
    .addField(prefix + 'slap', 'Sends a random anime slap image: _<' + prefix + 'slap @user>_')
    .addField(prefix + 'info', 'Sends bot info: _<' + prefix + 'info>_')
    .addField(prefix + 'ping', 'Bot ping: _<' + prefix + 'ping>_')
    .addField(prefix + 'poll', 'Creates a basic yes/no poll: _<' + prefix + 'poll question>_')
    .addField(prefix + 'meme', 'Sends an ancient meme: _<' + prefix + 'meme>_')
    .addField(prefix + 'kick', 'Kicks a user: _<' + prefix + 'kick @user reason>_')
    .addField(prefix + 'ban', 'Bans a user: _<' + prefix + 'ban @user reason>_')
    .addField(prefix + 'eval', 'executes any command, bot owner only: _<' + prefix + 'eval code>_')
    .addField(prefix + 'say', 'Says anything, bot owner only: _<' + prefix + 'say text>_')
message.channel.send({embed});
}

exports.help = {name: "help"}