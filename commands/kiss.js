const Discord = module.require('discord.js');
const prefix = (process.env.BOT_PREFIX)
const imgcfg = require("../images.json");
var imagearray = imgcfg.kissarray

module.exports.run = async (bot, message, args) => {
    let randomimage = imagearray[Math.floor(Math.random() * imagearray.length)];
    let whom = message.mentions.members.first();
    if (!whom) return message.reply('Usage: _<' + prefix + 'kiss @user>_');
    const embed = new Discord.RichEmbed()
    .setColor(0x005eff)
    .setDescription(`${message.author} **kissed** ${whom}`)
    .setImage(randomimage)
message.channel.send({embed});
}

exports.help = {name: "kiss"}