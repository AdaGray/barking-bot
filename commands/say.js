const Discord = module.require('discord.js');
const OwnerID = (process.env.BOT_OWNER);

module.exports.run = async (bot, message, args) => {
  if(message.author.id !== OwnerID) return message.reply("This command is bot owner only");
  if(!args[0]) return message.reply("Message can't be empty!");
  message.channel.send(args.join(" "))
}

exports.help = {name: "say"}