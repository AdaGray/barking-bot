const Discord = module.require('discord.js');
const OwnerID = (process.env.BOT_OWNER);
const prefix = (process.env.BOT_PREFIX)

module.exports.run = async (bot, message, args) => {
    const embed = new Discord.RichEmbed()
    .setColor(0x005eff)
    .setTitle('Info')
    .addField('BarkingBot', 'This is a multifunctional discord bot by BarkingDog#4975')
    .addField('Development', 'The bot is currently in alpha stage, not even beta.')
    .addField('Invite', "You can't invite this bot to your server **YET**, it is in development!")
    .addField('Node.js', 'Made using the discord.js library')
    .addField('Heroku', 'Hosted on heroku.com')
    .addField('GitHub', "Bot's Git repo: https://gitlab.com/BarkingDogMc/barking-bot/")
    .addField('Prefix', 'The current global prefix is: "' + prefix + '"')
    .addField('Owner', 'The bot owner is: <@' + OwnerID + '>')
    message.channel.send({embed});
}

exports.help = {name: "info"}