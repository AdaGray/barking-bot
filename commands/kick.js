const Discord = module.require('discord.js');
const prefix = (process.env.BOT_PREFIX)

module.exports.run = async (bot, message, args) => {
  if(!message.member.hasPermission("KICK_MEMBERS")) return message.reply("You are not a moderator"); // Checks for mod
  if(!args[0]) return message.reply("Usage: _<" + prefix + "kick [@user] [reason]>_"); // If no args, error.
  let kicked = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0])); // Sets who to kick
  let reason = args.join(" ").slice(22);  // Sets reason
  if(!reason) return message.reply("Usage: _<" + prefix + "kick [@user] [reason]>_"); // If no reason, error.
  if(kicked.hasPermission("KICK_MEMBERS")) return message.channel.send("Can't kick this user"); // If unkickable, error.

// Embed
  const embed = new Discord.RichEmbed()
  .setColor(0x005eff)
  .addField("Kicked: ", `${kicked}`)
  .addField("Kicked by: ", `<@${message.author.id}>`)
  .addField("Reason", `${reason}`);
message.channel.send({embed});

// Actual kick
message.guild.member(kicked).kick(reason);
}

exports.help = {name: "kick"}