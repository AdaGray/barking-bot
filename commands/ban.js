const Discord = module.require('discord.js');
const prefix = (process.env.BOT_PREFIX)

module.exports.run = async (bot, message, args) => {
  if(!message.member.hasPermission("BAN_MEMBERS")) return message.reply("You are not a moderator");
  if(!args[0]) return message.reply("Usage: _<" + prefix + "ban [@user] [reason]>_");
  let banned = message.guild.member(message.mentions.users.first() || message.guild.members.get(args[0]));
  let reason = args.join(" ").slice(22);
  if(!reason) return message.reply("Usage: _<" + prefix + "ban [@user] [reason]>_");
  if(banned.hasPermission("BAN_MEMBERS")) return message.channel.send("Can't ban this user");
//embed
  const embed = new Discord.RichEmbed()
  .setColor(0x005eff)
  .addField("Banned: ", `${banned}}`)
  .addField("Banned by: ", `<@${message.author.id}>`)
  .addField("Reason", `${reason}`);
message.channel.send({embed});
//bans
message.guild.member(banned).ban(reason);
}

exports.help = {name: "ban"}