const Discord = module.require('discord.js');
const OwnerID = (process.env.BOT_OWNER);
const prefix = (process.env.BOT_PREFIX)

module.exports.run = async (bot, message, args) => {
  if(!args[0]) return message.reply("Usage: _<" + prefix + "poll question>_");
  let question = args.slice(0).join(" ");
  const embed = new Discord.RichEmbed()
.setTitle(`${question}`)
.setColor(0x005eff)
.setDescription(`Poll by ${message.author.username}`)

message.channel.send({embed}).then(newMessage => {
    newMessage.react('✅')
    .then(() => newMessage.react('❎'))
    .catch(() => console.error('Poll command failed.'));
});
}


exports.help = {name: "poll"}