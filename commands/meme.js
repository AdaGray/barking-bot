const Discord = module.require('discord.js');
const imgcfg = require("../images.json");
var imagearray = imgcfg.memearray

module.exports.run = async (bot, message, args) => {
    let randomimage = imagearray[Math.floor(Math.random() * imagearray.length)];
    const embed = new Discord.RichEmbed()
    .setColor(0x005eff)
    .setDescription(`Here are the memes you requested, ${message.author}!`)
    .setImage(randomimage)
message.channel.send({embed});
}

exports.help = {name: "meme"}