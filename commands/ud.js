Discord = module.require('discord.js');
const snekfetch = require('snekfetch');
const urban = require('urban.js')

exports.run = async (bot, message, args) => {
    let word = args.join(" ");
    let udjson = urban(word);
    let definition = udjson.definition;
    let example = udjson.example;
    const embed = new Discord.RichEmbed()
    .setColor(0x005eff)
    .setTitle(word)
    .addField(definition, 'Examples:' + example)
message.channel.send({embed});
}

exports.help = {name: "ud"}
